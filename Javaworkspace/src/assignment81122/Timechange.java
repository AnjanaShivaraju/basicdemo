package assignment81122;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Timechange {

	public static void main(String[] args) throws Exception {
		Scanner sc=new Scanner(System.in);
		System.out.println( "enter 12 hour format time");
		String s=sc.nextLine();
		DateFormat  inFormat =new SimpleDateFormat("hh:mm:ssaa");
		DateFormat  outFormat =new SimpleDateFormat("HH:MM:SS");
				
		Date date=null;
		date=inFormat.parse(s);
		if (date!=null) {
			String myDate=outFormat.format(date);
			System.out.println(myDate);
		}
		sc.close();
	}

}
