package assignment81122;

import java.util.Scanner;

public class Petrol {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
        System.out.print("Enter the type of vehicle bike or car : ");
        String vehicle = in.nextLine();
        System.out.print("Enter the amount of petrol: ");
        int amount = in.nextInt();
        result(amount,vehicle);
    }
    public static void result(int amount, String vehicle)
    {
        int litres = amount/115;
        float distance=0;
        double miles;
        if(vehicle.equalsIgnoreCase("car"))
        {
            distance = 8*litres;
            miles=distance/1.6;
            }
        else if(vehicle.equalsIgnoreCase("bike"))
        {
            distance = 20*litres;
            miles=distance/1.6;
            }
        else
        {
            System.out.println("Enter valid vehicle type - (bike/car)");
            return;
            }
        System.out.println("Litres of petrol customer gets : "+litres);
        System.out.println("Distance customer can travel : "+distance);
        System.out.println("miles customer can travel : "+miles);
	}

}
